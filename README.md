## Api
- https://www.themoviedb.org/documentation/api

## User Story
- As a user, i should be able browse movies by categories such as upcoming, trending, top rated and popular movies 
- As a user, i should be able to see detail of movies such as movie poster, title, genere, description, rating, cast, and reviews
- As a user, i should be able to rate movie
- As a user, i should be able to add to favorites
- As a user, i should be able to watch trailer of movies once i navigate to detail of movies

## Design
- Either you can use your own creativity to design user interface or you reference any user interface in web.

## Tick Tock 🕐
- You will be provided the 2 weeks time for the completion of this assignment

## Have you completed assignment?
- Wow, please add me @khadkarajesh as collaborator
- We will drop the reviews over your repository

## What are we expecting from this assignment?
- Code Quality
- Programming standards
- Object oriented design patterns and principle
- Smell of code
- your style of working in team
- Code coverage
